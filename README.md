# ESP32 RFID
This program checks for four connected MFRC522 sensors if the correct card is placed. If the cards on all sensors are correct, a digital output pin of the ESP32 is switched.

For every sensor multiple correct cards can be specified (backups) for the case if a card gets lost.

# Hardware
I designed a [custom board](https://cadlab.io/project/25042/master/files) for this project.
